import { Intents } from 'discord.js';
import { config as configEnvironment } from 'dotenv';
import { AppClient } from './app/client';

configEnvironment();

const client = new AppClient({ intents: [Intents.FLAGS.GUILDS] });

client.login(process.env.DISCORD_TOKEN);
