import { Client, ClientOptions, Message } from 'discord.js';

export class AppClient extends Client {
  constructor(options: ClientOptions) {
    super(options);
    this.on('message', AppClient.messageHandler);
  }

  static messageHandler(msg: Message): void {
    console.log(msg.content);
  }
}
